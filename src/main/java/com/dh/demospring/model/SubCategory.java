package com.dh.demospring.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class SubCategory extends ModelBase {
    private String namec;
    private String code;
    @OneToOne(optional = false)
    private Category category;

    public String getNamec() {
        return namec;
    }

    public void setNamec(String namec) {
        this.namec = namec;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
