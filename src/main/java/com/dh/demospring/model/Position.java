package com.dh.demospring.model;

import javax.persistence.Entity;

@Entity
public class Position extends ModelBase {
    private String name;

    //Constructor por el generate seleccionado nombre
    public Position(String name) {
        this.name = name;
    }

    //Constructor por defecto seleccionado el segundo boton de abajo de los tres el del medio
    public Position() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
