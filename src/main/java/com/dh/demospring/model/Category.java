package com.dh.demospring.model;

import javax.persistence.Entity;

@Entity
public class Category extends ModelBase {
    private String namec;
    private String code;

    public String getNamec() {
        return namec;
    }

    public void setNamec(String namec) {
        this.namec = namec;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
